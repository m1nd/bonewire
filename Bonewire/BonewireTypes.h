#ifndef Types_H
#define Types_H

//
// Signed datatypes
//

#ifndef __int8_q_def
#define __int8_q_def
typedef char int8_q;
#endif

#ifndef __int16_q_def
#define __int16_q_def
typedef short int16_q;
#endif

#ifndef __int32_q_def
#define __int32_q_def
typedef int int32_q;
#endif

//
// Unsigned datatypes
//

#ifndef __uint8_q_def
#define __uint8_q_def
typedef unsigned char uint8_q;
#endif

#ifndef __uint16_q_def
#define __uint16_q_def
typedef unsigned short uint16_q;
#endif

#ifndef __uint32_q_def
#define __uint32_q_def
typedef unsigned int uint32_q;
#endif

//
// Others
//

#ifndef __byte_q_def
#define __byte_q_def
typedef uint8_q byte_q;
#endif

#endif // Types_H
