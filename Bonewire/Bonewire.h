#ifndef BONEWIRE_H
#define BONEWIRE_H

//
// Project include
//
#include "BonewireTypes.h"

//
// --------------
//  PinDirection
// --------------
//
enum PinDirection
{
  input = 0, output
};

//
// -------
//  Value
// -------
//
enum Value
{
  low = 0, high
};

//
// -----------
//  I2cAnswer
// -----------
//
enum I2cAnswer
{
  ACK = 0, NACK
};

//
// -----------
//  I2cDevice
// -----------
// I2C_0 (???)
// + SCL: ???
// + SDA: ???
//
// I2C_1 (works by default)
// + SCL: P9_19
// + SDA: P9_20
//
// I2C_2 (must be enabled)
// + SCL: P9_24
// + SDA: P9_26
//
enum I2cDevice
{
  I2C_0 = 0, I2C_1, I2C_2
};

//
// -----------
//  SpiDevice
// -----------
//
enum SpiDevice
{
  SPI_1_0 = 0, SPI_1_1, SPI_2_0, SPI_2_1
};

//
// -----
//  AIN
// -----
//
enum AIN
{
  AIN_0 = 0, AIN_1, AIN_2, AIN_3, AIN_4, AIN_5, AIN_6, AIN_7
};

//
// ------
//  GPIO
// ------
// All GPIOs without any comment should work fine by default.
// To use the others the proper device tree overlay has to be unloaded.
// This refers in particular onto the pins used by LCD (HDMI).
//
enum GPIO
{
  //
  // P9
  //
  GPIO_30 = 30,    // (used by UART)
  GPIO_60 = 60,
  GPIO_31 = 31,    // (used by UART)
  GPIO_50 = 50,    // (used by PWM)
  GPIO_48 = 48,
  GPIO_51 = 51,    // (used by PWM)
  GPIO_4 = 4,      // (used by SPI)
  GPIO_5 = 5,      // (used by SPI)
  GPIO_3 = 3,
  GPIO_2 = 2,
  GPIO_49 = 49,
  GPIO_15 = 15,
  GPIO_117 = 117,
  GPIO_14 = 14,
  GPIO_115 = 115,
  GPIO_123 = 123,  // (used by SPI)
  GPIO_121 = 121,  // (used by SPI)
  GPIO_122 = 122,  // (used by SPI)
  GPIO_120 = 120,  // (used by SPI)
  GPIO_20 = 20,
  GPIO_7 = 7,      // (used by PWM)

  //
  // P8
  //
  GPIO_38 = 38,  // (used by MMC)
  GPIO_39 = 39,  // (used by MMC)
  GPIO_34 = 34,  // (used by MMC)
  GPIO_35 = 35,  // (used by MMC)
  GPIO_66 = 66,
  GPIO_67 = 67,
  GPIO_69 = 69,
  GPIO_68 = 68,
  GPIO_45 = 45,
  GPIO_44 = 44,
  GPIO_23 = 23,  // (used by PWM)
  GPIO_26 = 26,
  GPIO_47 = 47,
  GPIO_46 = 46,
  GPIO_27 = 27,
  GPIO_65 = 65,
  GPIO_22 = 22,  // (used by PWM)
  GPIO_63 = 63,  // (used by MMC)
  GPIO_62 = 62,  // (used by MMC)
  GPIO_37 = 37,  // (used by MMC)
  GPIO_36 = 36,  // (used by MMC)
  GPIO_33 = 33,  // (used by MMC)
  GPIO_32 = 32,  // (used by MMC)
  GPIO_61 = 61,
  GPIO_86 = 86,  // (used by LCD)
  GPIO_88 = 88,  // (used by LCD)
  GPIO_87 = 87,  // (used by LCD)
  GPIO_89 = 89,  // (used by LCD)
  GPIO_10 = 10,  // (used by LCD)
  GPIO_11 = 11,  // (used by LCD)
  GPIO_9 = 9,    // (used by LCD)
  GPIO_81 = 81,  // (used by LCD)
  GPIO_8 = 8,    // (used by LCD)
  GPIO_80 = 80,  // (used by LCD)
  GPIO_78 = 78,  // (used by LCD)
  GPIO_79 = 79,  // (used by LCD)
  GPIO_76 = 76,  // (used by LCD)
  GPIO_77 = 77,  // (used by LCD)
  GPIO_74 = 74,  // (used by LCD)
  GPIO_75 = 75,  // (used by LCD)
  GPIO_72 = 72,  // (used by LCD)
  GPIO_73 = 73,  // (used by LCD)
  GPIO_70 = 70,  // (used by LCD)
  GPIO_71 = 71   // (used by LCD)
};

#endif // BONEWIRE_H
