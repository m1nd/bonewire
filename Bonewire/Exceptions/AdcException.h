#ifndef ADCEXCEPTION_H
#define ADCEXCEPTION_H

//
// Project include
//
#include "BonewireException.h"

class AdcException : public BonewireException
{
public:
  /**
   * Overloaded constructor
   */
  AdcException(const string message)
  {
    this->_message = message;
  }

  /**
   * Overloaded constructor
   */
  AdcException(const int32_q error)
  {
    this->_error = error;
  }

  /**
   * Overloaded constructor
   */
  AdcException(const string message, const int32_q error)
  {
    this->_message = message;
    this->_error = error;
  }

  /**
   * Default destructor
   */
  virtual ~AdcException()
  {
    // Left empty
  }
};

#endif // ADCEXCEPTION_H
