#ifndef SPIEXCEPTION_H
#define SPIEXCEPTION_H

//
// Project include
//
#include "BonewireException.h"

class SpiException : public BonewireException
{
public:
  /**
   * Overloaded constructor
   */
  SpiException(const string message)
  {
    this->_message = message;
  }

  /**
   * Overloaded constructor
   */
  SpiException(const int32_q error)
  {
    this->_error = error;
  }

  /**
   * Overloaded constructor
   */
  SpiException(const string message, const int32_q error)
  {
    this->_message = message;
    this->_error = error;
  }

  /**
   * Default destructor
   */
  virtual ~SpiException()
  {
    // Left empty
  }
};

#endif // SPIEXCEPTION_H
