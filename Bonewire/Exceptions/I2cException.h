#ifndef I2CEXCEPTION_H
#define I2CEXCEPTION_H

//
// Project include
//
#include "BonewireException.h"

class I2cException : public BonewireException
{
public:
  /**
   * Overloaded constructor
   */
  I2cException(const string message)
  {
    this->_message = message;
  }

  /**
   * Overloaded constructor
   */
  I2cException(const int32_q error)
  {
    this->_error = error;
  }

  /**
   * Overloaded constructor
   */
  I2cException(const string message, const int32_q error)
  {
    this->_message = message;
    this->_error = error;
  }

  /**
   * Default destructor
   */
  virtual ~I2cException()
  {
    // Left empty
  }
};

#endif // I2CEXCEPTION_H
