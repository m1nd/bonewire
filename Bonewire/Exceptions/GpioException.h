#ifndef GPIOEXCEPTION_H
#define GPIOEXCEPTION_H

//
// Project include
//
#include "BonewireException.h"

class GpioException : public BonewireException
{
public:
  /**
   * Overloaded constructor
   */
  GpioException(const string message)
  {
    this->_message = message;
  }

  /**
   * Overloaded constructor
   */
  GpioException(const int32_q error)
  {
    this->_error = error;
  }

  /**
   * Overloaded constructor
   */
  GpioException(const string message, const int32_q error)
  {
    this->_message = message;
    this->_error = error;
  }

  /**
   * Default destructor
   */
  virtual ~GpioException()
  {
    // Left empty
  }
};

#endif // GPIOEXCEPTION_H
