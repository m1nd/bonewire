#ifndef BONEWIREEXCEPTION_H
#define BONEWIREEXCEPTION_H

//
// System include
//
#include <string>

//
// Project include
//
#include "../Bonewire.h"

//
// Using
//
using std::string;

class BonewireException
{
protected:
  /**
   * Exception message
   */
  string _message;

  /**
   * Exception error code
   */
  int32_q _error;

public:
  /**
   * Default constructor
   */
  BonewireException()
      : _message("Exception message not set."), _error(0)
  {
  }

  /**
   * Overloaded constructor
   */
  BonewireException(const string message)
      : _message(message), _error(0)
  {
  }

  /**
   * Overloaded constructor
   */
  BonewireException(const int32_q error)
      : _message("Exception message not set."), _error(error)
  {
  }

  /**
   * Overloaded constructor
   */
  BonewireException(const string message, const int32_q error)
      : _message(message), _error(error)
  {
  }

  /**
   * Default destructor
   */
  virtual ~BonewireException()
  {
  }

  /**
   * Get the exception message.
   */
  string getMessage()
  {
    return _message;
  }

  /**
   * Get the exception error code.
   */
  int32_q getError()
  {
    return _error;
  }
};

#endif // BONEWIREEXCEPTION_H
