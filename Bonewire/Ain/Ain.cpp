#include "Ain.h"

using namespace Bonewire;

Ain::Ain(AIN ain)
    : _ain(ain)
{
  // Setup
  this->_loadDeviceTree();
}

Ain::~Ain()
{
  // Left empty
}

int32_q Ain::value()
{
  //
  // Path looks something like: /sys/devices/ocp.3/helper.15/AIN0
  //
  stringstream path;
  path << BASE_PATH << "/" << this->_getOcp() << "/" << this->_getHelper() << "/" << "AIN" << _ain;

  // Try to open the adc
  ifstream valueFile;
  valueFile.open(path.str().c_str());

  // Error check
  if (valueFile.fail())
  {
    valueFile.close();
    throw AdcException("Getting adc value failed.");
  }

  // Read
  int32_q ret = 0;
  valueFile >> ret;

  valueFile.close();

  return ret;
}

string Ain::_searchDirectory(string directory, string what)
{
  DIR *dir;
  dirent *entry;

  // Open the directory
  dir = opendir(directory.c_str());

  // Error check
  if (dir == nullptr)
    throw AdcException("Searching in directory failed.");

  // Iterate through all files in the
  // target directory
  string ret = "";
  while ((entry = readdir(dir)) != nullptr)
  {
    // Ignore hidden files
    if (entry->d_name[0] == '.')
      continue;

    // Check for match
    if (strstr(entry->d_name, what.c_str()) != nullptr)
    {
      ret = entry->d_name;
      break;
    }
  }

  closedir(dir);

  return ret;
}

string Ain::_getCapeManager()
{
  string capeManager = this->_searchDirectory(BASE_PATH, "bone_capemgr.");

  if (capeManager.empty())
    throw AdcException("Getting adc cape manager failed.");

  return capeManager;
}

string Ain::_getOcp()
{
  string ocp = this->_searchDirectory(BASE_PATH, "ocp.");

  if (ocp.empty())
    throw AdcException("Getting adc ocp failed.");

  return ocp;
}

string Ain::_getHelper()
{
  string helper = this->_searchDirectory(BASE_PATH + "/" + _getOcp(), "helper.");

  if (helper.empty())
    throw AdcException("Getting adc helper failed.");

  return helper;
}

void Ain::_loadDeviceTree()
{
  //
  // Path looks something like: /sys/devices/bone_capemgr.9/slots
  //
  stringstream path;
  path << BASE_PATH << "/" << this->_getCapeManager() << "/slots";

  // Try to open the device tree
  ofstream slotsFile;
  slotsFile.open(path.str().c_str());

  // Error check
  if (slotsFile.fail())
  {
    slotsFile.close();
    throw AdcException("Loading adc device tree failed.");
  }

  // Load device tree
  slotsFile << "cape-bone-iio";
  slotsFile.close();
}
