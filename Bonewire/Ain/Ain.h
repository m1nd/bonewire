#ifndef AIN_H
#define AIN_H

//
// System includes
//
#include <dirent.h>  // Directory listing
#include <cstring>   // std::strstr
#include <sstream>   // std::stringstream
#include <fstream>   // std::ofstream, std::ifstream

//
// Project includes
//
#include "../Bonewire.h"
#include "../Exceptions/AdcException.h"

//
// Using
//
using std::strstr;
using std::stringstream;
using std::ofstream;
using std::ifstream;

//
// Do not exceed 1.8V on any of the analog inputs!
//
//
// Example usage
//
/*
 Ain in(AIN_0);

 while(true)
 {
 std::cout << in.value() << std::endl;
 sleep(1);
 }
 */

namespace Bonewire
{
  class Ain
  {
  private:
    /**
     * Base path string
     */
    const string BASE_PATH = "/sys/devices";

    /**
     * AIN
     */
    AIN _ain;

    /**
     * Search something in the parsed directory.
     *
     * @param  directory Directory
     * @param  what      What to search
     * @return           Found item
     */
    string _searchDirectory(string directory, string what);

    /**
     * Retrieve cape manager path.
     *
     * @return Cape manager
     */
    string _getCapeManager();

    /**
     * Retrieve ocp path.
     *
     * @return Ocp
     */
    string _getOcp();

    /**
     * Retrieve helper path.
     *
     * @return Helper
     */
    string _getHelper();

    /**
     * Load adc device tree.
     *
     * @return void
     */
    void _loadDeviceTree();

    /**
     * Copy constructor
     */
    Ain(Ain &original);

    /**
     * Assignment operator
     */
    Ain &operator=(const Ain &source);

  public:
    /**
     * Overloaded constructor
     */
    Ain(AIN ain);

    /**
     * Default destructor
     */
    virtual ~Ain();

    /**
     * Get the Ain's value.
     *
     * @return Ain value
     */
    int32_q value();
  };
}

#endif // AIN_H
