#include "SoftwareI2c.h"

using namespace Bonewire;

SoftwareI2c::SoftwareI2c(Gpio *sda, Gpio *scl)
    : _sda(sda), _scl(scl)
{
  // Left empty
}

SoftwareI2c::~SoftwareI2c()
{
  // Left empty
}

void SoftwareI2c::start()
{
  // Drop SDA
  _sda->set(low);
  _scl->set(low);
}

void SoftwareI2c::restart()
{
  // Prepare
  _sda->set(high);
  _scl->set(high);

  // Drop SDA
  _sda->set(low);
  _scl->set(low);
}

void SoftwareI2c::stop()
{
  // Prepare
  _sda->set(low);
  _scl->set(high);

  // Raise SDA
  _sda->set(high);
}

void SoftwareI2c::write(byte_q data)
{
  // Set SDA direction to
  // write the data to the bus
  _sda->setDirection(output);

  // Write 8 data bits
  for (byte_q i = 0; i < 8; i++)
  {
    // Check MSB
    if (data & 0x80)
      _sda->set(high);
    else
      _sda->set(low);

    _clockOnce();

    // Left shift the data byte
    // to get the next data bit
    // |0|1|0|0|0|1|0|0| --> |1|0|0|0|1|0|0|0|
    data <<= 1;
  }

  // Clocking in the ACK bit
  _clockOnce();
}

//
// TODO Does not work as expected.
//
byte_q SoftwareI2c::read(I2cAnswer answer)
{
  byte_q data = 0;

  // Set SDA direction to
  // read the incoming data
  _sda->setDirection(input);

  // Read 8 data bits
  for (byte_q i = 0; i < 8; i++)
  {
    // OR data byte
    // |0|1|0|0|0|1|0|0|
    // |0|0|0|0|0|0|0|1|
    // -----------------
    // |0|1|0|0|0|1|0|1|
    if (_sda->isHigh())
      data |= 0x01;

    _clockOnce();

    // Left shift the data byte
    data <<= 1;
  }

  // Reset SDA direction
  _sda->setDirection(output);

  switch (answer)
  {
    case ACK:
      _sda->set(low);
      break;
    case NACK:
      _sda->set(high);
      break;
  }

  // Clocking in the (N)ACK bit
  _clockOnce();

  return data;
}

void SoftwareI2c::_clockOnce()
{
  //
  // Full CLK cycle: .. _|¯|_ ..
  //
  _scl->set(high);
  _scl->set(low);
}
