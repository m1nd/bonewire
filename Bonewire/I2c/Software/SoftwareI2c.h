#ifndef SOFTWAREI2C_H
#define SOFTWAREI2C_H

//
// Project include
//
#include "../../Gpio/Gpio.h"

//
// This is a software bit-banging
// implementation of the i2c protocol.
//
//
// Example usage
//
/*
 Gpio sda(GPIO_2, output);
 Gpio scl(GPIO_3, output);
 SoftwareI2c i2c(sda, scl);

 i2c.start();
 i2c.write(0xEE);
 i2c.write(0xF4);
 i2c.write(0x2E);
 i2c.stop();

 usleep(4.5 * 1000);

 i2c.start();
 i2c.write(0xEE);
 i2c.write(0xF6);
 i2c.restart();
 i2c.write(0xEF);
 byte_q msb = i2c.read(ACK);
 byte_q lsb = i2c.read(NACK);
 i2c.stop();
 */

namespace Bonewire
{
  class SoftwareI2c
  {
  private:
    /**
     * I2C SDA
     */
    Gpio *_sda;

    /**
     * I2C SCL
     */
    Gpio *_scl;

    /**
     * Clock SCL once.
     *
     * @return void
     */
    void _clockOnce();

    /**
     * Copy constructor
     */
    SoftwareI2c(SoftwareI2c &original);

    /**
     * Assignment operator
     */
    SoftwareI2c &operator=(const SoftwareI2c &source);

  public:
    /**
     * Overloaded constructor
     */
    SoftwareI2c(Gpio *sda, Gpio *scl);

    /**
     * Default destructor
     */
    virtual ~SoftwareI2c();

    /**
     * Send start condition.
     *
     * @return void
     */
    void start();

    /**
     * Send restart command.
     *
     * @return void
     */
    void restart();

    /**
     * Send stop condition.
     *
     * @return void
     */
    void stop();

    /**
     * Write data to i2c bus.
     *
     * @param data Data byte
     * @return     void
     */
    void write(byte_q data);

    /**
     * Read data from i2c bus.
     *
     * @param answer Answer from master to slave (ACK or NACK)
     * @return       Data byte
     */
    byte_q read(I2cAnswer answer);
  };
}

#endif // SOFTWAREI2C_H
