#include "HardwareI2c.h"

using namespace Bonewire;

HardwareI2c::HardwareI2c(I2cDevice device)
    : _bus(0), _device(device), _slaveAddress(0x00)
{
  // Open the communication
  // to the slave
  _setup();
}

HardwareI2c::HardwareI2c(const HardwareI2c &original)
{
  _bus = original._bus;
  _device = original._device;
  _slaveAddress = original._slaveAddress;
}

HardwareI2c::~HardwareI2c()
{
  // Close the i2c bus
  ::close(_bus);
}

HardwareI2c &HardwareI2c::operator<<(byte_q data)
{
  // Add data
  _dataBuffer.push_back(data);

  return *this;
}

HardwareI2c &HardwareI2c::operator<<(byte_q *data)
{
  // Get data length
  uint32_q length = std::char_traits<byte_q>::length(data);

  // Add data
  for (uint32_q i = 0; i < length; i++)
    _dataBuffer.push_back(data[i]);

  return *this;
}

void HardwareI2c::setSlaveAddress(byte_q slaveAddress)
{
  // Apply the slave address
  if (::ioctl(_bus, I2C_SLAVE, slaveAddress) < 0)
    throw I2cException("Failed to open the communication to the slave.");

  // Remember slave address
  _slaveAddress = slaveAddress;
}

void HardwareI2c::write()
{
  // Build i2c message
  struct i2c_msg message;
  message.addr = _slaveAddress;
  message.flags = 0;
  message.len = _dataBuffer.size();
  message.buf = _dataBuffer.data();

  // Build i2c message packet
  struct i2c_rdwr_ioctl_data packet;
  packet.msgs = &message;
  packet.nmsgs = 1;

  // Write packet to the i2c bus
  if (::ioctl(_bus, I2C_RDWR, &packet) < 0)
    throw I2cException("Writing data to the i2c bus failed.");

  // Cleanup
  _dataBuffer.clear();
}

byte_q HardwareI2c::read(byte_q address)
{
  // Build i2c write message
  struct i2c_msg messages[2];
  messages[0].addr = _slaveAddress;
  messages[0].flags = 0;
  messages[0].len = 1;
  messages[0].buf = &address;

  // Init data buffer
  byte_q buffer;

  // Build i2c read message
  messages[1].addr = _slaveAddress;
  messages[1].flags = I2C_M_RD;
  messages[1].len = sizeof(buffer);
  messages[1].buf = &buffer;

  // Build i2c message packet
  struct i2c_rdwr_ioctl_data packet;
  packet.msgs = messages;
  packet.nmsgs = 2;

  // Write packet to the i2c bus
  if (::ioctl(_bus, I2C_RDWR, &packet) < 0)
    throw I2cException("Reading data from the i2c bus failed.");

  return buffer;
}

byte_q *HardwareI2c::read(byte_q address, uint32_q length)
{
  // Build i2c write message
  struct i2c_msg messages[2];
  messages[0].addr = _slaveAddress;
  messages[0].flags = 0;
  messages[0].len = 1;
  messages[0].buf = &address;

  // Init data buffer
  byte_q *buffer = new byte_q[length];

  // Build i2c read message
  messages[1].addr = _slaveAddress;
  messages[1].flags = I2C_M_RD;
  messages[1].len = sizeof(buffer);
  messages[1].buf = buffer;

  // Build i2c message packet
  struct i2c_rdwr_ioctl_data packet;
  packet.msgs = messages;
  packet.nmsgs = 2;

  // Write packet to the i2c bus
  if (::ioctl(_bus, I2C_RDWR, &packet) < 0)
    throw I2cException("Reading data from the i2c bus failed.");

  return buffer;
}

void HardwareI2c::_setup()
{
  stringstream path;
  path << BASE_PATH << _device;

  // Open the i2c bus
  if ((_bus = ::open(path.str().c_str(), O_RDWR)) < 0)
    throw I2cException("Failed to open the I2C bus.");
}
