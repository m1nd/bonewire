#ifndef HARDWAREI2C_H
#define HARDWAREI2C_H

//
// System includes
//
#include <linux/i2c.h>      // i2c structs
#include <linux/i2c-dev.h>  // I2C_SLAVE
#include <sys/ioctl.h>      // ioctl()
#include <fcntl.h>          // O_RDWR
#include <unistd.h>         // open(), close(), read(), ..
#include <vector>           // std::vector
#include <sstream>          // std::stringstream

//
// Project includes
//
#include "../../Bonewire.h"
#include "../../Exceptions/I2cException.h"

//
// Using
//
using std::vector;
using std::stringstream;

//
// This is the hardware implementation
// of the i2c protocol.
//
// The max. speed is around 100kHz.
//
//
// Example usage
//
/*
 HardwareI2c i2c(I2C_1);
 i2c.setSlaveAddress(0x77);

 i2c << 0xF4 << 0x2E << 0xF6;
 i2c.write();
 usleep(4.5 * 1000);
 byte_q *dataRead = i2c.read(2);

 delete dataRead;
 */

namespace Bonewire
{
  class HardwareI2c
  {
  private:
    /**
     * Base path string
     */
    const string BASE_PATH = "/dev/i2c-";

    /**
     * I2C bus
     */
    int32_q _bus;

    /**
     * I2C device
     */
    I2cDevice _device;

    /**
     * Slave address
     */
    byte_q _slaveAddress;

    /**
     * Pending data array
     */
    vector<byte_q> _dataBuffer;

    /**
     * Setup the I2C bus.
     *
     * @return void
     */
    void _setup();

    /**
     * Copy constructor
     */
    HardwareI2c(const HardwareI2c &original);

    /**
     * Assignment operator
     */
    HardwareI2c &operator=(const HardwareI2c &original);

  public:
    /**
     * Overloaded constructor
     */
    HardwareI2c(I2cDevice device);

    /**
     * Default destructor
     */
    virtual ~HardwareI2c();

    /**
     * Add data to the data buffer.
     *
     * @param data Data byte
     * @return     Object reference
     */
    HardwareI2c &operator<<(byte_q data);

    /**
     * Add data to the data buffer.
     *
     * @param data Data bytes
     * @return     Object reference
     */
    HardwareI2c &operator<<(byte_q *data);

    /**
     * Set the i2c slaves address.
     *
     * @param slaveAddress Slave address
     * @return             void
     */
    void setSlaveAddress(byte_q slaveAddress);

    /**
     * Write the pending data to the i2c bus.
     *
     * @return void
     */
    void write();

    /**
     * Read a data byte from the i2c bus.
     *
     * @param address Register address
     * @return        Data byte
     */
    byte_q read(byte_q address);

    /**
     * Read several data bytes from the i2c bus.
     *
     * @param address Register start address
     * @param length  Read length
     * @return        Data bytes
     */
    byte_q *read(byte_q address, uint32_q length);
  };
}

#endif // HARDWAREI2C_H
