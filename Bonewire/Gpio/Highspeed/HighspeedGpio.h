#ifndef HIGHSPEEDGPIO_H
#define HIGHSPEEDGPIO_H

//
// System includes
//
#include <sys/mman.h>  // mmap()
#include <fcntl.h>     // O_RDWR, O_SYNC

//
// Project includes
//
#include "../Gpio.h"
#include "../../Exceptions/GpioException.h"

//
// USE AT YOUR OWN RISK
//
// I'm not responsible for any damage
// this code may cause on your device!
//
//
// GPIO used via this implementation clock
// at a max. speed of approximately 1.3MHz
//
//
// -- References --
// Articles:
// http://vabi-robotics.blogspot.co.at/2013/10/register-access-to-gpios-of-beaglebone.html?m=1
// http://www.alexanderhiam.com/tutorials/beaglebone-io-using-python-mmap/
//
// ARM335x datasheet:
// http://www.ti.com/lit/ug/spruh73k/spruh73k.pdf
//
//
// Example usage
//
/*
 Gpio pin(GPIO_2, output);

 while(true)
 {
 pin.toogleValue();
 sleep(1);
 }
 */

//
// Defines
//
// The offsets are divided by 4 because
// the beaglebone uses a 32-bit ARM architecture
// so the datatypes are defined as long.
// But the memory array can only be accessed
// via a byte offset: long / 4 = byte
//
#define PIN_CONFIG       0x134 / 4
#define DATA_IN          0x138 / 4
#define DATA_OUT         0x13C / 4
#define GPIO0_CONTROLLER 0x44E07000
#define GPIO1_CONTROLLER 0x4804C000
#define GPIO2_CONTROLLER 0x481AC000
#define GPIO3_CONTROLLER 0x481AE000

namespace Bonewire
{
  class HighspeedGpio : public Gpio
  {
  private:
    /**
     * GPIO memory pointer
     */
    uint32_q *_mem;

    /**
     * Get a pointer to the target GPIO controller
     * in the memory.
     *
     * @param controller GPIO controller
     * @return           void
     */
    void _getMemoryPointer(uint32_q controller);

    /**
     * Copy constructor
     */
    HighspeedGpio(HighspeedGpio &original);

    /**
     * Assignment operator
     */
    HighspeedGpio &operator=(const HighspeedGpio &source);

  public:
    /**
     * Overloaded constructor
     */
    HighspeedGpio(GPIO pin, PinDirection direction);

    /**
     * Overloaded constructor
     */
    HighspeedGpio(uint8_q pin, PinDirection direction);

    /**
     * Default destructor
     */
    virtual ~HighspeedGpio();

    /**
     * Set the Gpio direction.
     *
     * @param  direction Gpio direction
     * @return           void
     */
    void setDirection(PinDirection direction);

    /**
     * Toggle the Gpio direction.
     *
     * @return void
     */
    void toggleDirection();

    /**
     * Get the Gpio's value.
     *
     * @return Gpio value
     */
    Value value();

    /**
     * Get the Gpio's numeric value.
     *
     * @return Numeric Gpio value
     */
    uint8_q numericValue();

    /**
     * Set the Gpio value.
     *
     * @param  value Gpio value
     * @return       void
     */
    void set(Value value);

    /**
     * Toggle the Gpio's value.
     *
     * @return void
     */
    void toggleValue();

    /**
     * Check if the Gpio is digital low.
     *
     * @return State
     */
    bool isLow();

    /**
     * Check if the Gpio is digital high.
     *
     * @return State
     */
    bool isHigh();
  };
}

#endif // HIGHSPEEDGPIO_H
