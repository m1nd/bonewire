#include "HighspeedGpio.h"

using namespace Bonewire;

HighspeedGpio::HighspeedGpio(GPIO pin, PinDirection direction)
    : _mem(nullptr)
{
  // Setup
  if (pin <= 32)
    _getMemoryPointer(GPIO0_CONTROLLER);
  else if (pin > 32 && pin <= 64)
    _getMemoryPointer(GPIO1_CONTROLLER);
  else if (pin > 64 && pin <= 96)
    _getMemoryPointer(GPIO2_CONTROLLER);
  else if (pin > 96)
    _getMemoryPointer(GPIO3_CONTROLLER);

  // Get the real pin number for
  // the corresponding controller
  this->_pin = pin % 32;
  this->setDirection(direction);
}

HighspeedGpio::HighspeedGpio(uint8_q pin, PinDirection direction)
    : _mem(nullptr)
{
  // Setup
  if (pin <= 32)
    _getMemoryPointer(GPIO0_CONTROLLER);
  else if (pin > 32 && pin <= 64)
    _getMemoryPointer(GPIO1_CONTROLLER);
  else if (pin > 64 && pin <= 96)
    _getMemoryPointer(GPIO2_CONTROLLER);
  else if (pin > 96)
    _getMemoryPointer(GPIO3_CONTROLLER);

  // Get the real pin number for
  // the corresponding controller
  this->_pin = pin % 32;
  this->setDirection(direction);
}

HighspeedGpio::~HighspeedGpio()
{
  // Left empty
}

void HighspeedGpio::setDirection(PinDirection direction)
{
  switch (direction)
  {
    case input:
      _mem[PIN_CONFIG] |= (1 << _pin);
      break;
    case output:
      _mem[PIN_CONFIG] &= ~(1 << _pin);
      break;
  }

  // Remember new direction
  this->_direction = direction;
}

void HighspeedGpio::toggleDirection()
{
  switch (this->_direction)
  {
    case input:
      setDirection(output);
      break;
    case output:
      setDirection(input);
      break;
  }
}

Value HighspeedGpio::value()
{
  // Check if bit is set
  if (_mem[DATA_IN] & (1 << _pin))
    return high;

  return low;
}

uint8_q HighspeedGpio::numericValue()
{
  // Return bit
  return _mem[DATA_IN] & (1 << _pin);
}

void HighspeedGpio::set(Value value)
{
  switch (value)
  {
    case low:
      _mem[DATA_OUT] &= ~(1 << _pin);
      break;
    case high:
      _mem[DATA_OUT] |= (1 << _pin);
      break;
  }
}

void HighspeedGpio::toggleValue()
{
  // XOR to toggle the bit
  _mem[DATA_OUT] ^= (1 << _pin);
}

bool HighspeedGpio::isLow()
{
  // Check if the bit is set
  if (_mem[DATA_IN] & (1 << _pin))
    return false;

  return true;
}

bool HighspeedGpio::isHigh()
{
  // Check if the bit is set
  if (_mem[DATA_IN] & (1 << _pin))
    return true;

  return false;
}

void HighspeedGpio::_getMemoryPointer(uint32_q controller)
{
  // Open mem file
  int32_q memory = open("/dev/mem", O_RDWR | O_SYNC);

  //
  // Get a pointer to a GPIO controller in the memory
  //
  // nullptr:    The OS can put the memory map where it wants
  // 0x1000:     Size (bytes) --> 0x1000 = 4096
  // PROT..:     Read and write access
  // MAP_SHARED: Don't get a copy of the map
  // memory:     mem file descriptor
  // controller: Address where the pointer should point to
  this->_mem = (uint32_q *) mmap(nullptr, 0x1000, PROT_READ | PROT_WRITE, MAP_SHARED, memory, controller);

  // Error check
  if (this->_mem == MAP_FAILED)
    throw GpioException("Failed to retrieve memory pointer.");
}
