#ifndef GPIO_H
#define GPIO_H

//
// System includes
//
#include <sstream>  // std::stringstream
#include <fstream>  // std::ofstream, std::ifstream

//
// Project includes
//
#include "../Bonewire.h"
#include "../Exceptions/GpioException.h"

//
// Using
//
using std::stringstream;
using std::ofstream;
using std::ifstream;

//
// GPIO used via this implementation clock
// at a max. speed of approximately 3.5kHz
//
//
// Example usage
//
/*
 Gpio pin(GPIO_2, output);

 while(true)
 {
 pin.toogleValue();
 sleep(1);
 }
 */

namespace Bonewire
{
  class Gpio
  {
  private:
    /**
     * Base path string
     */
    const string BASE_PATH = "/sys/class/gpio";

    /**
     * Export the target pin.
     *
     * @return void
     */
    void _export();

    /**
     * Unexport the target pin.
     *
     * @return void
     */
    void _unexport();

    /**
     * Assignment operator
     */
    Gpio &operator=(const Gpio &source);

  protected:
    /**
     * Pin
     */
    uint32_q _pin;

    /**
     * Direction
     */
    PinDirection _direction;

    /**
     * Default constructor
     */
    Gpio();

  public:
    /**
     * Overloaded constructor
     */
    Gpio(GPIO pin, PinDirection direction);

    /**
     * Overloaded constructor
     */
    Gpio(uint32_q pin, PinDirection direction);

    /**
     * Copy constructor
     */
    Gpio(Gpio &original);

    /**
     * Default destructor
     */
    virtual ~Gpio();

    /**
     * Set the Gpio direction.
     *
     * @param  direction Gpio direction
     * @return           void
     */
    virtual void setDirection(PinDirection direction);

    /**
     * Toggle the Gpio direction.
     *
     * @return void
     */
    virtual void toggleDirection();

    /**
     * Get the Gpio's value.
     *
     * @return Gpio value
     */
    virtual Value value();

    /**
     * Get the Gpio's numeric value.
     *
     * @return Numeric Gpio value
     */
    virtual uint8_q numericValue();

    /**
     * Set the Gpio value.
     *
     * @param  value Gpio value
     * @return       void
     */
    virtual void set(Value value);

    /**
     * Toggle the Gpio's value.
     *
     * @return void
     */
    virtual void toggleValue();

    /**
     * Check if the Gpio is digital low.
     *
     * @return State
     */
    virtual bool isLow();

    /**
     * Check if the Gpio is digital high.
     *
     * @return State
     */
    virtual bool isHigh();
  };
}

#endif // GPIO_H
