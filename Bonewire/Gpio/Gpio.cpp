#include "Gpio.h"

using namespace Bonewire;

Gpio::Gpio()
    : _pin(0), _direction(input)
{
  // Left empty
}

Gpio::Gpio(GPIO pin, PinDirection direction)
    : _pin(pin), _direction(direction)
{
  // Setup
  this->_export();
  this->setDirection(direction);
}

Gpio::Gpio(uint32_q pin, PinDirection direction)
    : _pin(pin), _direction(direction)
{
  // Setup
  this->_export();
  this->setDirection(direction);
}

Gpio::Gpio(Gpio &original)
{
  this->_pin = original._pin;
  this->_direction = original._direction;
}

Gpio::~Gpio()
{
  try
  {
    // Cleanup
    this->_unexport();
  }
  catch (BonewireException &e)
  {
    // Ignore any exception for proper stack-unwinding
  }
}

void Gpio::setDirection(PinDirection direction)
{
  //
  // Path looks something like: /sys/class/gpio/gpio10/direction
  //
  stringstream path;
  path << BASE_PATH << "/gpio" << this->_pin << "/direction";

  // Try to open the GPIO
  ofstream directionFile;
  directionFile.open(path.str().c_str());

  // Error check
  if (directionFile.fail())
  {
    directionFile.close();
    throw GpioException("Setting GPIO direction failed.");
  }

  // Set direction
  switch (direction)
  {
    case input:
      directionFile << "in";
      break;
    case output:
      directionFile << "out";
      break;
  }

  // Remember new direction
  this->_direction = direction;

  directionFile.close();
}

void Gpio::toggleDirection()
{
  switch (this->_direction)
  {
    case input:
      setDirection(output);
      break;
    case output:
      setDirection(input);
      break;
  }
}

Value Gpio::value()
{
  //
  // Path looks something like: /sys/class/gpio/gpio10/value
  //
  stringstream path;
  path << BASE_PATH << "/gpio" << this->_pin << "/value";

  // Try to open GPIO
  ifstream valueFile;
  valueFile.open(path.str().c_str());

  // Error check
  if (valueFile.fail())
  {
    valueFile.close();
    throw GpioException("Retrieving GPIO value failed.");
  }

  // Read
  string ret;
  valueFile >> ret;

  valueFile.close();

  if (ret == "1")
    return high;

  return low;
}

uint8_q Gpio::numericValue()
{
  return static_cast<uint8_q>(this->value());
}

void Gpio::set(Value value)
{
  //
  // Path looks something like: /sys/class/gpio/gpio10/value
  //
  stringstream path;
  path << BASE_PATH << "/gpio" << this->_pin << "/value";

  // Try to open GPIO
  ofstream valueFile;
  valueFile.open(path.str().c_str());

  // Error check
  if (valueFile.fail())
  {
    valueFile.close();
    throw GpioException("Setting GPIO value failed.");
  }

  // Set value
  switch (value)
  {
    case low:
      valueFile << "0";
      break;
    case high:
      valueFile << "1";
      break;
  }

  valueFile.close();
}

void Gpio::toggleValue()
{
  switch (this->value())
  {
    case low:
      this->set(high);
      break;
    case high:
      this->set(low);
      break;
  }
}

bool Gpio::isLow()
{
  if (this->value() == low)
    return true;

  return false;
}

bool Gpio::isHigh()
{
  if (this->value() == high)
    return true;

  return false;
}

void Gpio::_export()
{
  //
  // Path looks something like: /sys/class/gpio/export
  //
  stringstream path;
  path << BASE_PATH << "/export";

  // Try to open export
  ofstream exportFile;
  exportFile.open(path.str().c_str());

  // Error check
  if (exportFile.fail())
  {
    exportFile.close();
    throw GpioException("Setting up GPIO failed.");
  }

  // Export GPIO
  exportFile << this->_pin;
  exportFile.close();
}

void Gpio::_unexport()
{
  //
  // Path looks something like: /sys/class/gpio/unexport
  //
  stringstream path;
  path << BASE_PATH << "/unexport";

  // Try to open unexport
  ofstream unexportFile;
  unexportFile.open(path.str().c_str());

  // Error check
  if (unexportFile.fail())
  {
    unexportFile.close();
    throw GpioException("Cleaning up GPIO failed.");
  }

  // Unexport GPIO
  unexportFile << this->_pin;
  unexportFile.close();
}
