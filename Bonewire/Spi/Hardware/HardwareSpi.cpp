#include "HardwareSpi.h"

using namespace Bonewire;

HardwareSpi::HardwareSpi(SpiDevice device, HighspeedGpio *slaveSelect)
    : _bus(0), _device(device), _slaveSelect(slaveSelect)
{
  // Setup the spi bus
  _setup();
}

HardwareSpi::~HardwareSpi()
{
  // Close the spi bus
  ::close(_bus);
}

HardwareSpi &HardwareSpi::operator<<(byte_q data)
{
  // Add data
  _dataBuffer.push_back(data);

  return *this;
}

HardwareSpi &HardwareSpi::operator<<(byte_q *data)
{
  // Get data length
  uint32_q length = std::char_traits<byte_q>::length(data);

  // Add data
  for (uint32_q i = 0; i < length; i++)
    _dataBuffer.push_back(data[i]);

  return *this;
}

byte_q *HardwareSpi::transfer(uint32_q bytesToRead)
{
  // Set custom slave select
  if (_slaveSelect != nullptr)
    _slaveSelect->set(low);

  // Initialize transfer struct
  // Important: Do a memset (without it won't work)
  struct spi_ioc_transfer tr[2];
  memset(&tr, 0, sizeof(tr));

  // Write struct
  tr[0].tx_buf = (uint32_q) _dataBuffer.data();
  tr[0].rx_buf = (uint32_q) nullptr;
  tr[0].len = _dataBuffer.size();

  // Keep the chip select line low
  // during the transfer
  tr[0].cs_change = 0;

  //
  // Write + read
  //
  if (bytesToRead > 0)
  {
    // Buffer for the data that will be received
    byte_q *buffer = new byte_q[bytesToRead];

    // Read struct
    tr[1].tx_buf = (uint32_q) nullptr;
    tr[1].rx_buf = (uint32_q) buffer;
    tr[1].len = bytesToRead;

    // Transfer data
    if (::ioctl(_bus, SPI_IOC_MESSAGE(2), &tr) < 0)
      throw SpiException("Failed to read data from the spi bus.");

    // Cleanup
    _dataBuffer.clear();

    // Reset custom slave select
    if (_slaveSelect != nullptr)
      _slaveSelect->set(high);

    return buffer;
  }
  //
  // Write only
  //
  else
  {
    // Write data
    if (::ioctl(_bus, SPI_IOC_MESSAGE(1), &tr) < 0)
      throw SpiException("Failed to write data to the spi bus.");

    // Cleanup
    _dataBuffer.clear();

    // Reset custom slave select
    if (_slaveSelect != nullptr)
      _slaveSelect->set(high);

    return nullptr;
  }
}

void HardwareSpi::setSpeed(uint32_q speed)
{
  this->_speed = speed;

  // Set SPI write configuration
  if (::ioctl(_bus, SPI_IOC_WR_MAX_SPEED_HZ, &_speed) < 0)
    throw SpiException("Failed to set SPI write configuration [speed].");

  // Set SPI read configuration
  if (::ioctl(_bus, SPI_IOC_RD_MAX_SPEED_HZ, &_speed) < 0)
    throw SpiException("Failed to set SPI read configuration [speed].");
}

void HardwareSpi::setDelay(uint32_q delay)
{
  this->_delay = delay;
}

void HardwareSpi::setBitsPerWord(uint32_q bitsPerWord)
{
  this->_bitsPerWord = bitsPerWord;

  // Set SPI write configuration
  if (::ioctl(_bus, SPI_IOC_WR_BITS_PER_WORD, &_bitsPerWord) < 0)
    throw SpiException("Failed to set SPI write configuration [bits per word].");

  // Set SPI read configuration
  if (::ioctl(_bus, SPI_IOC_RD_BITS_PER_WORD, &_bitsPerWord) < 0)
    throw SpiException("Failed to set SPI read configuration [bits per word].");
}

void HardwareSpi::setMode(byte_q mode)
{
  this->_mode = mode;

  // Set SPI write configuration
  if (::ioctl(_bus, SPI_IOC_WR_MODE, &_mode) < 0)
    throw SpiException("Failed to set SPI write mode.");

  // Set SPI read configuration
  if (::ioctl(_bus, SPI_IOC_RD_MODE, &_mode) < 0)
    throw SpiException("Failed to set SPI read mode.");
}

void HardwareSpi::_setup()
{
  stringstream path;
  path << BASE_PATH;

  // Get the device
  switch (this->_device)
  {
    case SPI_1_0:
      path << "1.0";
      break;
    case SPI_1_1:
      path << "1.1";
      break;
    case SPI_2_0:
      path << "2.0";
      break;
    case SPI_2_1:
      path << "2.1";
      break;
  }

  // Open the spi bus
  if ((_bus = ::open(path.str().c_str(), O_RDWR)) < 0)
    throw SpiException("Failed to open the SPI bus.");

  //
  // SPI write configuration
  //
  if (::ioctl(_bus, SPI_IOC_WR_MODE, &_mode) < 0)
    throw SpiException("Failed to set SPI write mode.");

  if (::ioctl(_bus, SPI_IOC_WR_BITS_PER_WORD, &_bitsPerWord) < 0)
    throw SpiException("Failed to set SPI write configuration [bits per word].");

  if (::ioctl(_bus, SPI_IOC_WR_MAX_SPEED_HZ, &_speed) < 0)
    throw SpiException("Failed to set SPI write configuration [speed].");

  //
  // SPI read configuration
  //
  if (::ioctl(_bus, SPI_IOC_RD_MODE, &_mode) < 0)
    throw SpiException("Failed to set SPI read mode.");

  if (::ioctl(_bus, SPI_IOC_RD_BITS_PER_WORD, &_bitsPerWord) < 0)
    throw SpiException("Failed to set SPI read configuration [bits per word].");

  if (::ioctl(_bus, SPI_IOC_RD_MAX_SPEED_HZ, &_speed) < 0)
    throw SpiException("Failed to set SPI read configuration [speed].");
}
