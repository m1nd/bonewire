#ifndef HARDWARESPI_H
#define HARDWARESPI_H

//
// System includes
//
#include <fcntl.h>             // O_RDWR
#include <sys/ioctl.h>         // ioctl()
#include <linux/spi/spidev.h>  // SPI constants
#include <vector>              // std::vector
#include <unistd.h>            // open(), close()
#include <string>              // std::string
#include <sstream>             // std::stringstream
#include <string.h>            // memset()

//
// Project includes
//
#include "../../Bonewire.h"
#include "../../Exceptions/SpiException.h"
#include "../../Gpio/Highspeed/HighspeedGpio.h"

//
// Using
//
using std::vector;
using std::string;
using std::stringstream;

//
// Example usage
//
/*
 HardwareSpi spi(SPI_1_0);
 spi.setSpeed(5000000);
 spi.setDelay(0);
 spi.setBitsPerWord(8);

 spi << 0x03 << 0x04;
 byte_q *data = spi.transfer(1);

 std::cout << std::bitset<8>(data[0]) << std::endl;
 */

namespace Bonewire
{
  class HardwareSpi
  {
  private:
    /**
     * Base path string
     */
    const string BASE_PATH = "/dev/spidev";

    /**
     * I2C bus
     */
    int32_q _bus;

    /**
     * SPI device
     */
    SpiDevice _device;

    /**
     * Custom slave select
     */
    HighspeedGpio *_slaveSelect;

    /**
     * Data buffer
     */
    vector<byte_q> _dataBuffer;

    /**
     * Speed in Hz
     */
    uint32_q _speed = 1000000;

    /**
     * Delay in us
     */
    uint32_q _delay = 0;

    /**
     * Bits per word
     */
    uint32_q _bitsPerWord = 8;

    /**
     * Mode
     *
     * Reference:
     * http://de.wikipedia.org/wiki/Serial_Peripheral_Interface
     */
    byte_q _mode = SPI_MODE_0;

    /**
     * Setup the SPI bus.
     *
     * @return void
     */
    void _setup();

    /**
     * Copy constructor
     */
    HardwareSpi(HardwareSpi &original);

    /**
     * Assignment operator
     */
    HardwareSpi &operator=(const HardwareSpi &source);

  public:
    /**
     * Overloaded constructor
     */
    HardwareSpi(SpiDevice device, HighspeedGpio *slaveSelect = nullptr);

    /**
     * Default destructor
     */
    virtual ~HardwareSpi();

    /**
     * Add data to the data buffer.
     *
     * @param data Data byte
     * @return     Object reference
     */
    HardwareSpi &operator<<(byte_q data);

    /**
     * Add data to the data buffer.
     *
     * @param data Data bytes
     * @return     Object reference
     */
    HardwareSpi &operator<<(byte_q *data);

    /**
     * Transfer data on the spi bus.
     *
     * bytesToRead == 0: If bytesToRead is 0 only a write operation
     * will be performed.
     * bytesToRead  > 0: If bytesToRead is greater than 0 a transfer
     * (write + read) will be performed.
     *
     * @param bytesToRead How many bits will be read
     * @return            Data array or NULL
     */
    byte_q *transfer(uint32_q bytesToRead);

    /**
     * Set the SPI bus speed.
     *
     * @param speed Speed
     * @return      void
     */
    void setSpeed(uint32_q speed);

    /**
     * Set the SPI bus delay.
     *
     * @param delay Delay
     * @return      void
     */
    void setDelay(uint32_q delay);

    /**
     * Set the SPI bus bits per word.
     *
     * @param bitsPerWord Bits per word
     * @return            void
     */
    void setBitsPerWord(uint32_q bitsPerWord);

    /**
     * Set the SPI bus mode.
     *
     * @param mode Mode
     * @return     void
     */
    void setMode(byte_q mode);

    /**
     * Get the SPI bus speed.
     *
     * @return Speed
     */
    uint32_q getSpeed()
    {
      return this->_speed;
    }

    /**
     * Set the SPI bus delay.
     *
     * @return Delay
     */
    uint32_q getDelay()
    {
      return this->_delay;
    }

    /**
     * Set the SPI bus bits per word.
     *
     * @return Bits per word
     */
    uint32_q getBitsPerWord()
    {
      return this->_bitsPerWord;
    }

    /**
     * Set the SPI bus mode.
     *
     * @return Mode
     */
    byte_q getMode()
    {
      return this->_mode;
    }
  };
}

#endif // HARDWARESPI_H
